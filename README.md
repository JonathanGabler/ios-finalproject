# IOS-FinalProject

CS 4220 GPA Calculator Project
## User Stories
1. I can enter my current GPA and total number of credit hours that have a letter grade.
For example, 80 credit hours with a current GPA of 3.25. (S, U, EX, EX-F, Y grades do not
count in a GPA total).
2. I can add courses that I am currently taking (or am planning to take) to a list. I need to
be able to give the course a name, input how many credit hours it is worth, and
optionally assign it a projected grade from the list of UMSL grades (A, A-, B+, etc) at the
bottom of this document.
3. I can see the total grade points for each course in the list (credit hours multiplied by the
projected grade, if I have assigned it one). For example, I should see 12 grade points for
a 3 credit hour class in which I received an A grade.
4. I can see my projected GPA based on my current GPA, current credit hours, and the
projected grades I have entered.
5. I can substitute a projected grade for a previous grade. In this case, I should be able to
say that a particular course is a substitute course, tell the program what my previous
grade was in that course and what my projected grade is. It should adjust my projected
GPA appropriately. (The grade points and credit hours would need to be removed for
the current GPA and the projected grade would be used.)
6. I can a delete a course from the list.

## Technical Requirements
1. USE MVC: That means you need a Model file that handles the logic of tracking all the
courses and calculating the needed GPA values.
2. USE AUTOLAYOUT: You can use stack views to make it a lot easier, but you need basic
constraints to the app could be used on several different sizes of device.
3. Use a table view for the course list.
4. Tapping on a table view cell should take the user to a separate view controller where
they can edit/update the course information.
5. Have a button somewhere (probably in the navigation bar) to add new courses to the
list. When the user taps on the button, segue to a separate view controller to enterinformation on the new course. (This could be the same view controller that is used to
edit/update a course).
6. Use a separate view controller to enter/update the user’s current GPA and current
credit hours.
7. There are a couple ways you can delete table view cells. You’ll have to research the
ways and pick one. But the common methods are: using table view edit row actions
(swiping left on a cell could bring up a button that says “Delete” for example), or using
the table view’s editing mode triggered by some button press (the iOS Clock app uses
this method as an example). I would recommend working on this delete functionality
only after you have implemented the rest of the user stories.

## Notes
1. The “Previous Grade” list (for substituting a grade) should be C-, D+, D, D-, F, and FN.
2. The formula for a GPA is the sum of all grade points divided by credit hours.


### Optional Features
These are optional but will increase the likelihood that UMSL would use your app.
1. Persist the current GPA, current credit hours, and course list to device, so the user does
not have to reenter the data if the app is terminated or the device is turned off.
2. Add a “What GPA would I need to get X GPA?” feature. A user would enter the
remaining credit hours and their desired overall GPA at the end of their program. Then
the app would tell them the average GPA they need to achieve in their remaining credit
hours.

### UMSL Grades and Grade Values
* A 4.0
* A- 3.7
* B+ 3.3
* B 3.0
* B- 2.7
* C+ 2.3
* C 2.0C- 1.7
* D+ 1.3
* D 1.0
* D- 0.7
* F 0
* FN 0